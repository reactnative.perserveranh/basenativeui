export default {
  TOP_HEADLINES: '/v2/top-headlines',

  EVERY_THING: '/v2/everything',

  SOURCES: '/v2/sources',

  BACKUP: 'https://nep5wallet.com/api/backup-nep5',

  GETDATATOKEN: 'https://api.neoscan.io/api/main_net/v1/get_balance/',
};
