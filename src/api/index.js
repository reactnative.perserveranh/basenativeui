import Api from './Api';
import ApiInstance from './ApiModule';

export const getBnb = () => {
  //
};

export const backUp = data => {
  return ApiInstance.post(Api.BACKUP, data);
};

export const getData = data => {
  const {address} = data.params;
  let paramurl = '';
  if (address) {
    paramurl += `${address}`;
  }
  const url = Api.GETDATATOKEN + `${paramurl}`;
  return ApiInstance.get(url, data);
};
