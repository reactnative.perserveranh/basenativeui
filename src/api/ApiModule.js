import axios from 'axios';

class ApiModule {
  constructor() {
    const service = axios.create({
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      },
    });
    service.interceptors.response.use(this.handleSuccess, this.handleError);
    this.service = service;
  }

  get(endpoint, payload) {
    return this.service.request({
      method: 'GET',
      url: endpoint,
      responseType: 'json',
      data: payload,
    });
  }

  post(endpoint, payload) {
    return this.service.request({
      method: 'POST',
      url: endpoint,
      responseType: 'json',
      data: payload,
    });
  }
}

export default new ApiModule();
