import React from 'react';
import {
  FlatList,
  Animated,
  ScrollView,
  Dimensions,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {Block, Text, theme} from 'galio-framework';
import QRCode from 'react-native-qrcode-svg';
import nowTheme from '../../../constants/Theme';
import styles from './style';

const TAG = 'SETTING';
const {width} = Dimensions.get('screen');
class Settings extends React.Component {
  state = {
    text: 'http://facebook.github.io/react-native/',
  };

  renderCards = () => {
    const {navigation, dataItem} = this.props;
    const {address} = dataItem;

    this.scrollX = new Animated.Value(0);
    const imgContainer = [
      styles.imageContainer,
      styles.horizontalStyles,
      styles.shadow,
      styles.fullImage,
      styles.horizontalImage,
    ];
    const titleStyles = [styles.cardTitle];
    const cardContainer = [styles.card, styles.shadow];
    return (
      <Block flex style={styles.group}>
        <ScrollView
          horizontal={true}
          style={styles.contentContainer}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          scrollEventThrottle={16}
          onScroll={Animated.event([
            {nativeEvent: {contentOffset: {x: this.scrollX}}},
          ])}
          contentContainerStyle={{
            width: width,
          }}>
          <Block row={false} card flex style={cardContainer}>
            <TouchableWithoutFeedback>
              <Block flex middle style={imgContainer}>
                <QRCode value={address} size={300} color="#111d5e" />
              </Block>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback>
              <Block flex style={styles.cardDescription}>
                <Block flex center>
                  <Text
                    style={{fontFamily: 'montserrat-regular'}}
                    size={14}
                    style={titleStyles}
                    color={nowTheme.COLORS.SECONDARY}>
                    Address
                  </Text>
                  <Block flex center>
                    <Text
                      style={{
                        fontFamily: 'montserrat-regular',
                        textAlign: 'center',
                        padding: 15,
                      }}
                      size={14}
                      color={'#9A9A9A'}>
                      {address}
                    </Text>
                  </Block>
                </Block>
              </Block>
            </TouchableWithoutFeedback>
          </Block>
        </ScrollView>
      </Block>
    );
  };

  render() {
    return (
      <Block flex center>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.settings}>
          <FlatList
            // data={recommended}
            keyExtractor="1"
            // renderItem={this.renderItem}
            ListHeaderComponent={
              <Block center style={styles.title}>
                <Text
                  style={{
                    fontFamily: 'Montserrat-BoldItalic',
                    paddingBottom: 5,
                    margin: 10,
                  }}
                  size={theme.SIZES.BASE}
                  color={nowTheme.COLORS.TEXT}>
                  Recommended QRCode
                </Text>
                <Text
                  style={{
                    fontFamily: 'montserrat-regular',
                    textAlign: 'center',
                    margin: 10,
                  }}
                  size={12}
                  color={nowTheme.COLORS.CAPTION}
                  color={nowTheme.COLORS.TEXT}>
                  This address only supports NEP5 Token, Please check the token
                  platform before sending.
                </Text>
              </Block>
            }
          />
          {this.renderCards()}
        </ScrollView>
      </Block>
    );
  }
}

const mapStateToProps = state => {
  console.log(TAG + ' mapStateToProps ' + JSON.stringify(state));
  return {
    dataItem: state.default.dataItem.param,
  };
};

const mapDispatchToProps = dispatch => {
  console.log(TAG + ' mapDispatchToProps ');
  return {};
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Settings);
