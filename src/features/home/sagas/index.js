import {takeEvery} from 'redux-saga/effects';
import * as types from '../actionTypes';
import getdataToken from './homeSaga';

export const homeSagas = [takeEvery(types.GETDATA, getdataToken)];
