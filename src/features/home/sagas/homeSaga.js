import {call, put} from 'redux-saga/effects';
import {getdataFailed, getdataSuccess} from '../actions';
import {getData} from '../../../api';

export default function* getdataToken({payload}) {
  const params = {...payload};
  try {
    const resp = yield call(getData, params);
    const {data, status} = resp;
    const tmpData = data.data;
    if (status === 200) {
      yield put(getdataSuccess(tmpData));
    } else {
      yield put(getdataFailed(data));
    }
  } catch (error) {
    yield put(getdataFailed(error));
  }
}
