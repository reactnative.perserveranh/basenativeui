import * as types from './actionTypes';

const TAG = 'GetToken-action';

export function getData(params) {
  console.log(TAG + ' getData() ');
  return {
    type: types.GETDATA,
    payload: {
      params,
    },
  };
}

export function getdataSuccess(data) {
  console.log(TAG + ' getdataSuccess() ');
  return {
    type: types.GETDATA_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getdataFailed(error) {
  console.log(TAG + ' getdataFailed() error= ' + JSON.stringify(error));
  return {
    type: types.GETDATA_FAILED,
    payload: {
      error,
    },
  };
}
