import React from 'react';
import {FlatList, ActivityIndicator, View, Text} from 'react-native';
import {Block, theme, Button} from 'galio-framework';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {ListItem, SearchBar, Overlay} from 'react-native-elements';
import TouchableScale from 'react-native-touchable-scale'; // https://github.com/kohver/react-native-touchable-scale
import LinearGradient from 'react-native-linear-gradient'; // Only if no expo
import * as itemAction from '../../actions';
import styles from './style';
import {nowTheme} from '../../../constants';

const TAG = 'HOME';
const api = 'https://api.neoscan.io/api/main_net/v1/get_balance/';
let myInterval = null;
class Home extends React.Component {
  state = {
    loading: false,
    data: [],
    error: null,
    refreshing: false,
    search: '',
    dataSearch: [{name: 'Empty', id: 1, description: 'Not exist Token !'}],
  };
  componentDidMount() {
    this.getDataToken();
    setInterval(() => {
      this.getDataToken();
    }, 8000);
  }

  getDataToken = async () => {
    const {dataItem, navigation, onSetToken} = this.props;
    console.log('dataItem', dataItem);
    const {address} = dataItem;
    let data = null;
    const params = {};
    const tmpAddress = navigation.state.address || address;
    // const tmpAddress = 'AQ5XH4MHPyQy58s8VPpE95b2FVJTCVgcnE';
    const url = api + `${tmpAddress}`;
    this.setState({loading: true});
    data = await fetch(url);
    data = await data.json();
    if (onSetToken) {
      params.dataTokens = data.balance;
      await onSetToken(params);
    }
    this.setState({
      data: data.balance,
      loading: false,
    });
  };

  // handleRefresh = () => {
  //   this.setState(
  //     {
  //       refreshing: true,
  //     },
  //     () => {
  //       this.getDataToken();
  //     },
  //   );
  // };

  // handleLoadMore = () => {
  //   const {refreshing} = this.state;
  //   if (refreshing === true) {
  //     this.getDataToken();
  //   }
  // };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  updateSearch = search => {
    this.setState({search}, () => {
      const {data, search} = this.state;
      const tmpData = [...new Set(data.map(el => el.asset))];
      this.setState({
        dataSearch: data.filter(el => el.asset.includes(search)),
      });
    });
  };

  renderHeader = () => {
    const {search} = this.state;
    return (
      <SearchBar
        placeholder="Type Here..."
        onChangeText={this.updateSearch}
        value={search}
        round
        containerStyle={{
          backgroundColor: 'transparent',
          marginVertical: 12,
          borderBottomColor: nowTheme.COLORS.WHITE,
          borderTopColor: nowTheme.COLORS.WHITE,
        }}
        lightTheme
      />
    );
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        {/* <ActivityIndicator animating size="large" /> */}
      </View>
    );
  };

  render() {
    const {data, search, dataSearch} = this.state;
    const dataNull = [{name: 'Empty', id: 1, description: 'Not exist Token !'}];
    if (dataSearch.length === 0) {
      return (
        <Block flex style={styles.home}>
          <FlatList
            data={dataNull}
            renderItem={({item}) => (
              <ListItem
                Component={TouchableScale}
                friction={90} //
                tension={80} //
                activeScale={0.8}
                linearGradientProps={{
                  colors: ['#de1616', '#da7070', '#de1616'],
                  start: {x: 0.0, y: 0.25},
                  end: {x: 0.5, y: 1.0},
                }}
                ViewComponent={LinearGradient} // Only if no expo
                title={item.name}
                titleStyle={{color: 'white', fontWeight: 'bold'}}
                subtitleStyle={{color: 'white'}}
                subtitle={item.description}
                chevron={{color: 'white'}}
              />
            )}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
            ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            // onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            // onEndReached={this.handleLoadMore}
            onEndReachedThreshold={50}
          />
        </Block>
      );
    }
    return (
      <Block flex style={styles.home}>
        <FlatList
          data={search.length > 0 ? dataSearch : data}
          renderItem={({item}) => (
            <ListItem
              Component={TouchableScale}
              friction={90} //
              tension={80} //
              activeScale={0.8}
              linearGradientProps={{
                colors: ['#6780d5', '#6780d5', '#6780d5'],
                start: {x: 0.0, y: 0.25},
                end: {x: 0.5, y: 1.0},
              }}
              ViewComponent={LinearGradient} // Only if no expo
              title={`${item.asset}` + ' : ' + `${item.asset_symbol}`}
              titleStyle={{color: 'white', fontWeight: 'bold'}}
              subtitle={'Amount : ' + `${item.amount}`}
              subtitleStyle={{color: 'white'}}
              containerStyle={{borderBottomWidth: 0, marginVertical: 4}}
            />
          )}
          keyExtractor={item => item.asset_hash}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          // onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          // onEndReached={this.handleLoadMore}
          onEndReachedThreshold={50}
        />
      </Block>
    );
  }
}

const mapStateToProps = state => {
  console.log(TAG + ' mapStateToProps ' + JSON.stringify(state));
  return {
    dataItem: state,
  };
};

const mapDispatchToProps = dispatch => {
  console.log(TAG + ' mapDispatchToProps ');
  return {
    onSetItem: params => dispatch(itemAction.setItem(params)),
    onSetToken: params => dispatch(itemAction.setToken(params)),
  };
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Home);
