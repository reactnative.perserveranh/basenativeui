import {StyleSheet, Dimensions} from 'react-native';
import {theme} from 'galio-framework';

const {width} = Dimensions.get('screen');

export default StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
    paddingHorizontal: 2,
    fontFamily: 'montserrat-regular',
  },
});
