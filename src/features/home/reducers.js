import * as types from './actionTypes';

const TAG = 'getDataReducer';

const initialState = {
  showLoading: false,
  datas: [],
};

const getdatareducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GETDATA: {
      console.log(
        TAG + ' ' + types.GETDATA + ' state= ' + JSON.stringify(state),
      );
      return {
        ...state,
        showLoading: true,
      };
    }
    case types.GETDATA_SUCCESS: {
      console.log(
        TAG + ' ' + types.GETDATA_SUCCESS + ' state= ' + JSON.stringify(state),
      );
      return {
        ...state,
        datas: [...action.payload.data],
        showLoading: false,
      };
    }
    case types.GETDATA_FAILED: {
      console.log(
        TAG + ' ' + types.GETDATA_FAILED + ' state= ' + JSON.stringify(state),
      );
      return {
        ...state,
        showLoading: false,
      };
    }
    default:
      return state;
  }
};

export default getdatareducer;
