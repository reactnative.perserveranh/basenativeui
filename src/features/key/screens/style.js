import {StyleSheet, Dimensions, Platform} from 'react-native';
import {theme} from 'galio-framework';

import {nowTheme} from '../../../constants';

const {width, height} = Dimensions.get('screen');

export default StyleSheet.create({
  imageBackgroundContainer: {
    width: width,
    height: height,
    padding: 0,
    zIndex: 1,
  },
  imageBackground: {
    width: width,
    height: height,
  },
  registerContainer: {
    marginTop: 55,
    width: width * 0.9,
    height: height < 812 ? height * 0.6 : height * 0.6,
    backgroundColor: nowTheme.COLORS.WHITE,
    borderRadius: 4,
    shadowColor: nowTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: 'hidden',
  },
  socialConnect: {
    backgroundColor: nowTheme.COLORS.WHITE,
    // borderBottomWidth: StyleSheet.hairlineWidth,
    // borderColor: 'rgba(136, 152, 170, 0.3)',
  },
  socialButtons: {
    width: 80,
    height: 40,
    backgroundColor: '#fff',
    shadowColor: nowTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
  },
  socialTextButtons: {
    color: nowTheme.COLORS.PRIMARY,
    fontWeight: '800',
    fontSize: 14,
  },
  inputIcons: {
    marginRight: 12,
    color: nowTheme.COLORS.ICON_INPUT,
  },
  inputs: {
    borderWidth: 1,
    borderColor: '#E3E3E3',
    borderRadius: 21.5,
  },
  passwordCheck: {
    paddingLeft: 2,
    paddingTop: 6,
    paddingBottom: 5,
  },
  createButton: {
    width: width / 2,
    // marginTop: 10,
    marginBottom: 20,
  },
  social: {
    width: theme.SIZES.BASE * 3.5,
    height: theme.SIZES.BASE * 3.5,
    borderRadius: theme.SIZES.BASE * 1.75,
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  MainContainer: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    margin: 10,
  },

  TextInputStyleClass: {
    textAlign: 'left',
    borderWidth: 2,
    borderColor: '#9E9E9E',
    borderRadius: theme.SIZES.BASE / 2,
    backgroundColor: '#FFFFFF',
    height: 100,
    justifyContent: 'space-around',
    paddingHorizontal: 8,
  },
});
