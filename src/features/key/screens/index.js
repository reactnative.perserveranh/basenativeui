import React from 'react';
import {
  ImageBackground,
  Dimensions,
  TouchableWithoutFeedback,
  Keyboard,
  View,
  TextInput,
  Platform,
  AsyncStorage,
  Alert,
} from 'react-native';
import {Block, Text} from 'galio-framework';
import {connect} from 'react-redux';
import {compose} from 'redux';

import * as itemAction from '../../actions';
import {Button, Input} from '../../../components';
import {Images, nowTheme} from '../../../constants';
import getIconType from '../../../helper/getIconType';
import {navigateCreatePassword} from '../../../navigation/NavigationHelpers';

const neonJs = require('@cityofzion/neon-js');

const Neon = neonJs.default;
const query = Neon.create.query();

const {wallet} = neonJs;
const Icon = getIconType('Ionicon');
const {width} = Dimensions.get('screen');
import styles from './style';

const DismissKeyboard = ({children}) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

const TAG = 'KEY';
class Key extends React.Component {
  state = {
    privateKey: '',
    password: '',
    error: '',
  };

  onChangeText = value => {
    const {length} = value;
    if (value && length !== 52 && length !== 64) {
      this.setState({
        error: 'Incorrect key',
      });
    } else {
      this.setState({
        error: '',
      });
    }
    this.setState({
      privateKey: value,
    });
  };

  handleCreatePassword = async () => {
    const {privateKey} = this.state;
    const {onSetItem} = this.props;
    let account = {};
    const params = {};
    try {
      account = await new wallet.Account(privateKey);
      if (account && account.privateKey) {
        const {privateKey, address} = account;
        params.address = address;
        params.privateKey = privateKey;
        await AsyncStorage.setItem('paramAccount', JSON.stringify(params));
        if (onSetItem) {
          await onSetItem(params);
          await navigateCreatePassword(params);
        }
      }
    } catch (e) {
      Alert.alert(
        `Hmm, that's not the right password and key. Please try again`,
      );
    }
  };

  render() {
    const {privateKey, error} = this.state;
    return (
      <DismissKeyboard>
        <Block flex middle>
          <ImageBackground
            source={Images.RegisterBackground}
            style={styles.imageBackgroundContainer}
            imageStyle={styles.imageBackground}>
            <Block flex middle>
              <Block style={styles.registerContainer}>
                <Block flex space="evenly">
                  <Block flex={0.4} middle style={styles.socialConnect}>
                    <Block flex={0.2} middle>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontWeight: 'bold',
                        }}
                        color="#333"
                        size={24}>
                        Unlock With Key
                      </Text>
                    </Block>
                  </Block>

                  <Block flex={0.8} middle space="between">
                    <Block center flex={0.7}>
                      <Block flex space="between" center>
                        <Block>
                          <Block width={width * 0.8}>
                            <View style={styles.MainContainer}>
                              <TextInput
                                style={styles.TextInputStyleClass}
                                underlineColorAndroid="transparent"
                                placeholder={'Fill in the private key.'}
                                placeholderTextColor={'#9E9E9E'}
                                numberOfLines={4}
                                multiline={true}
                                onChangeText={text => this.onChangeText(text)}
                                value={privateKey}
                              />
                            </View>
                          </Block>
                        </Block>
                        <Text muted color="red">
                          {error}
                        </Text>
                        <Block center>
                          <Button
                            color="primary"
                            round
                            disabled={error !== '' || privateKey.length === 0}
                            style={styles.createButton}
                            onPress={this.handleCreatePassword}>
                            <Text
                              style={{fontFamily: 'Montserrat-BoldItalic'}}
                              size={14}
                              color={nowTheme.COLORS.WHITE}>
                              Unlock Wallet
                            </Text>
                          </Button>
                        </Block>
                      </Block>
                    </Block>
                  </Block>
                </Block>
              </Block>
            </Block>
          </ImageBackground>
        </Block>
      </DismissKeyboard>
    );
  }
}

const mapStateToProps = state => {
  console.log(TAG + ' mapStateToProps ' + JSON.stringify(state));
  return {
    dataItem: state,
  };
};

const mapDispatchToProps = dispatch => {
  console.log(TAG + ' mapDispatchToProps ');
  return {
    onSetItem: params => dispatch(itemAction.setItem(params)),
  };
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Key);
