import {put, delay} from 'redux-saga/effects';
import {Alert} from 'react-native';
import * as loginActions from '../actions';
import {navigateToHome} from '../../../navigation/NavigationHelpers';

export default function* fetchLogin() {
  yield put(loginActions.enableLoader());

  // call api
  // const response= yield call...
  yield delay(500);
  const response = {
    success: true,
    data: {id: 1},
  };

  console.log('responseLogin', response);
  if (response.success) {
    yield put(loginActions.onLoginResponse(response.data));
    yield put(loginActions.disableLoader({}));
    // yield call(navigateToMainAppScreens);
    navigateToHome();
  } else {
    yield put(loginActions.loginFailed());
    yield put(loginActions.disableLoader({}));
    setTimeout(() => {
      Alert.alert('Login fail!');
    }, 200);
  }
}
