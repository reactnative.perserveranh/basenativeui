import React from 'react';
import {
  ImageBackground,
  Image,
  StatusBar,
  Dimensions,
  View,
  TouchableOpacity,
  Button as ButtonNative,
  Alert,
  AsyncStorage,
} from 'react-native';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {Block, Button, Text, theme, Input, Checkbox} from 'galio-framework';

import {
  navigateAccount,
  navigateKey,
  navigateToHome,
} from '../../../navigation/NavigationHelpers';
import {Images, nowTheme} from '../../../constants';
import * as loginActions from '../actions';
import * as itemAction from '../../actions';
import styles from './style';
import getIconType from '../../../helper/getIconType';

const TAG = 'LoginScreen-log';
const Icon = getIconType('Ionicon');
const {height, width} = Dimensions.get('screen');

const Separator = () => {
  return <View style={styles.separator} />;
};

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: 'Abc123@@',
      error: '',
      nameKey: 'Privatekey',
    };
  }
  onChangeLogin = value => {
    this.setState({
      password: value,
    });
  };

  handleChangeKey = value => {
    if (value === true) {
      this.setState({
        nameKey: 'Privatekey',
      });
    } else {
      this.setState({
        nameKey: 'Keystore',
      });
    }
  };

  loginClick = async () => {
    const {onSetItem, onBackup} = this.props;
    const paramAccount = await AsyncStorage.getItem('paramAccount');
    if (!paramAccount) {
      this.setState({error: 'Incorrect password'});
      return;
    }
    const {password = '', address, privateKey, key} = JSON.parse(paramAccount);
    const tmpPassword = this.state.password;
    if (tmpPassword == password) {
      const params = {};
      params.privateKey = privateKey;
      params.keyStore = key;
      params.address = address;
      params.password = tmpPassword;
      if (onSetItem) {
        if (onBackup) {
          params.private = privateKey;
          params.type = 1;
          await onBackup(params);
        }
        await onSetItem(params);
        await navigateToHome(params);
      }
      this.setState({
        error: '',
        password: '',
      });
    } else {
      this.setState({error: 'Incorrect password'});
    }
    // this.props.onLogin('nghia', '123456');
  };

  handleCreateWallet = () => {
    navigateAccount({id: 1});
  };

  handleRecover = () => {
    navigateAccount({id: 2});
  };

  handleNavigateKey = () => {
    const {nameKey} = this.state;
    const param = {};
    if (nameKey == 'Privatekey') {
      param.id = 1;
      navigateKey(param);
    } else {
      Alert.alert('Please install extension when unlock with Keystore !');
    }
  };

  render() {
    const {error, nameKey, password} = this.state;
    const btnname = 'Unlock With ' + nameKey;
    return (
      <Block flex style={styles.container}>
        <StatusBar barStyle="light-content" />
        <Block flex>
          <ImageBackground
            source={Images.Onboarding}
            style={{flex: 1, height: height, width, zIndex: 1}}
          />
          <Block space="between" style={styles.padded}>
            <Block>
              <Block middle>
                <Image
                  source={Images.NowLogo}
                  style={{
                    width: 220,
                    height: 160,
                    bottom: 160,
                    position: 'absolute',
                  }}
                />
              </Block>
              <Block>
                <Block middle>
                  <Text
                    style={{
                      fontFamily: 'montserrat-regular',
                      bottom: 30,
                      position: 'absolute',
                      letterSpacing: 2,
                      paddingHorizontal: 20,
                      textAlign: 'center',
                    }}
                    color="white"
                    size={44}>
                    My Holder Wallet
                  </Text>
                </Block>
              </Block>
              {/* <Block middle row> */}
              {/* <Text
                  color="white"
                  size={16}
                  style={{fontFamily: 'Montserrat-BoldItalic'}}>
                  Designed by Perserver Anh !!! (.)(.) ...
                </Text> */}
              {/* <Image
                  source={Images.InvisionLogo}
                  style={{
                    height: 28,
                    width: 91,
                    marginLeft: theme.SIZES.BASE,
                  }}
                /> */}
              {/* </Block> */}
              <Block middle row style={{marginBottom: 10}}>
                <Text
                  color="white"
                  size={16}
                  style={{fontFamily: 'montserrat-regular'}}>
                  Coded by AIBB Team
                </Text>
                <Image
                  source={Images.CreativeTimLogo}
                  style={{
                    height: 29,
                    width: 129,
                    marginLeft: theme.SIZES.BASE,
                  }}
                />
              </Block>
              <Block>
                <Input
                  password
                  viewPass
                  placeholder="Password"
                  onChangeText={value => this.onChangeLogin(value)}
                  placeholderTextColor="#4F8EC9"
                  borderless
                  style={{borderColor: 'red'}}
                  help={<Text style={styles.bottomHelp}>{error}</Text>}
                  bottomHelp
                  Icon={
                    <Icon
                      size={20}
                      color={nowTheme.COLORS.ICON}
                      name="ios-checkmark"
                    />
                  }
                  value={password}
                />
              </Block>

              <Block
                row
                style={{
                  marginTop: theme.SIZES.BASE * 1.5,
                  marginBottom: theme.SIZES.BASE * 2,
                }}>
                <Button
                  shadowless
                  style={styles.button}
                  color={nowTheme.COLORS.PRIMARY}
                  onPress={this.loginClick}>
                  <Text
                    style={{
                      fontFamily: 'Montserrat-BoldItalic',
                      fontSize: 14,
                    }}
                    color={theme.COLORS.WHITE}>
                    GET STARTED
                  </Text>
                </Button>
              </Block>
              <Block flex style={[styles.group]}>
                <Block style={{marginHorizontal: theme.SIZES.BASE / 2}}>
                  <Block row space="between">
                    <TouchableOpacity
                      style={styles.btnBottom}
                      onPress={this.handleNavigateKey}>
                      <Text style={styles.txtBottom}>{btnname} </Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity
                      style={styles.btnBottom}
                      onPress={this.handleRecover}>
                      <Text style={styles.txtBottom}>Recover Wallet </Text>
                    </TouchableOpacity> */}
                    <Checkbox
                      color="success"
                      initialValue={true}
                      labelStyle={{color: '#FF9C09'}}
                      onChange={value => this.handleChangeKey(value)}
                    />
                  </Block>
                </Block>
              </Block>
              <Separator />
              <Block
                flex
                style={[styles.group, {paddingBottom: theme.SIZES.BASE * 5}]}>
                <Block style={{marginHorizontal: theme.SIZES.BASE / 2}}>
                  <Block row space="between">
                    <TouchableOpacity
                      style={styles.btnBottom}
                      onPress={this.handleCreateWallet}>
                      <Text style={styles.txtBottom}> Create New Wallet </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.btnBottom}
                      onPress={this.handleRecover}>
                      <Text style={styles.txtBottom}>Recover Wallet </Text>
                    </TouchableOpacity>
                  </Block>
                </Block>
              </Block>
            </Block>
          </Block>
        </Block>
      </Block>
    );
  }
}

const mapStateToProps = state => {
  console.log(TAG + ' mapStateToProps ' + JSON.stringify(state));
  return {
    status: state,
    dataItem: state,
  };
};

const mapDispatchToProps = dispatch => {
  console.log(TAG + ' mapDispatchToProps ');
  return {
    onSetItem: params => dispatch(itemAction.setItem(params)),
    onBackup: params => dispatch(itemAction.backUp(params)),
  };
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(LoginScreen);
