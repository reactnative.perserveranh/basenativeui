import {StyleSheet, Dimensions, Platform} from 'react-native';
import {theme} from 'galio-framework';

const {width, height} = Dimensions.get('window');
import {HeaderHeight} from '../../../constants/utils';

export default StyleSheet.create({
  container: {
    backgroundColor: theme.COLORS.BLACK,
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
  },
  padded: {
    paddingHorizontal: theme.SIZES.BASE * 2,
    zIndex: 3,
    position: 'absolute',
    bottom:
      Platform.OS === 'android' ? -theme.SIZES.BASE - 60 : theme.SIZES.BASE * 3,
  },
  button: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0,
  },
  otherLinksContainer: {
    paddingTop: height < 600 ? 5 : Platform.OS === 'android' ? 10 : 15,
    flexDirection: 'row',
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  gradient: {
    zIndex: 1,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 66,
  },
  bottomHelp: {
    color: 'red',
    fontSize: 20,
    marginTop: 15,
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  btnBottom: {
    opacity: 0.9,
    fontWeight: 'bold',
    fontSize: Platform.OS === 'android' ? 12 : 12,
    // borderBottomWidth: 1,
    paddingVertical: theme.SIZES.BASE - 5,
  },
  txtBottom: {
    color: theme.COLORS.WHITE,
    fontSize: Platform.OS === 'android' ? 14 : 15,
    // fontWeight: 'bold',
  },
});
