import * as types from './actionTypes';

const TAG = 'LoginReducer';

const initialState = {
  isLoggedIn: false,
  id: -1,
  username: '',
  password: '',
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN_REQUEST: {
      console.log(
        TAG + ' ' + types.LOGIN_REQUEST + ' state= ' + JSON.stringify(state),
      );
      return {
        ...state,
        username: action.username,
        password: action.password,
      };
    }
    case types.LOGIN_RESPONSE: {
      console.log(
        TAG + ' ' + types.LOGIN_RESPONSE + ' state= ' + JSON.stringify(state),
      );
      return {
        ...state,
        id: action.response.id,
      };
    }
    case types.LOGIN_FAILED: {
      console.log(
        TAG + ' ' + types.LOGIN_FAILED + ' state= ' + JSON.stringify(state),
      );
      return {
        ...state,
        isLoggedIn: false,
      };
    }
    default:
      return state;
  }
};

export default loginReducer;
