import {call, put, takeLatest, takeEvery} from 'redux-saga/effects';
import {
  setItemSuccess,
  backUpSuccess,
  backUpFailed,
  setTokenSuccess,
} from '../actions';
import {backUp} from '../../api';
import * as types from '../constants';

function* processBackUp({payload}) {
  const {param} = payload;
  try {
    const resp = yield call(backUp, param);
    const {data, status} = resp;
    if (status === 200) {
      yield put(backUpSuccess(data));
    } else {
      yield put(backUpFailed(data));
    }
  } catch (error) {
    yield put(backUpFailed(error));
  }
}

function* processSetToken({payload}) {
  const {param} = payload;
  yield put(setTokenSuccess(param));
}

function* processSetItem({payload}) {
  const {param} = payload;
  yield put(setItemSuccess(param));
}

function* itemSaga() {
  yield takeLatest(types.BACKUP, processBackUp);
  yield takeLatest(types.SET_ITEM, processSetItem);
  yield takeEvery(types.SET_TOKEN, processSetToken);
}

export default itemSaga;
