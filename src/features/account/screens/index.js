import React from 'react';
import {
  ImageBackground,
  Dimensions,
  TouchableWithoutFeedback,
  Keyboard,
  View,
  TextInput,
  Alert,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {Block, Text} from 'galio-framework';

import {Button, Input} from '../../../components';
import {Images, nowTheme} from '../../../constants';
import getIconType from '../../../helper/getIconType';
import {
  navigateBackup,
  navigateToHome,
} from '../../../navigation/NavigationHelpers';
import styles from './style';
import * as itemAction from '../../actions';

const neonJs = require('@cityofzion/neon-js');
const PasswordValidator = require('password-validator');

const Neon = neonJs.default;
const query = Neon.create.query();

const {wallet} = neonJs;

const schema = new PasswordValidator();
const schema2 = new PasswordValidator();
const schema3 = new PasswordValidator();
schema3
  .has()
  .uppercase()
  .has()
  .lowercase();
schema2.is().min(8);
schema
  .is()
  .max(100) // Maximum length 100
  .has()
  .digits() // Must have digits
  .has()
  .symbols()
  .has()
  .not()
  .spaces() // Should not have spaces
  .is()
  .not()
  .oneOf(['Passw0rd', 'Password123']);

const Icon = getIconType('Ionicon');
const {width} = Dimensions.get('screen');
const TAG = 'ACCOUNT';

const DismissKeyboard = ({children}) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class Account extends React.Component {
  state = {
    privateKey: '',
    password: '',
    confirmPassword: '',
    error: '',
    errorConfirm: '',
    isLoading: false,
  };

  onChangeText = value => {
    this.setState({
      privateKey: value,
    });
  };

  onChangeConfirmPassword = value => {
    this.setState(
      {
        confirmPassword: value,
      },
      () => {
        const {confirmPassword, password} = this.state;
        if (confirmPassword !== password) {
          this.setState({
            errorConfirm: 'Two passwords that you enter is inconsistent!',
          });
        } else {
          this.setState({
            errorConfirm: '',
          });
        }
      },
    );
  };

  onChangePassword = value => {
    this.setState(
      {
        password: value,
      },
      () => {
        if (value && !schema2.validate(value)) {
          this.setState({
            error: 'Password must be at least 8 characters',
          });
        } else if (value && !schema3.validate(value)) {
          this.setState({
            error: 'Password must contain atleast one capital',
          });
        } else if (value && !schema.validate(value)) {
          this.setState({
            error: 'Password must contain a special character',
          });
        } else {
          this.setState({
            error: '',
          });
        }
      },
    );
  };

  handleBackup = async () => {
    this.setState({
      isLoading: true,
    });
    const {error, errorConfirm, password} = this.state;
    const {onSetItem, onBackup} = this.props;
    if (!error && !errorConfirm) {
      try {
        const priv = Neon.create.privateKey();
        const account = new wallet.Account(priv);
        if (account) {
          const {address, privateKey} = account;
          await account.encrypt(password);
          const newKey = await account.export();

          const params = {};
          params.password = password;
          params.address = address;
          params.privateKey = privateKey;
          params.key = newKey.key;

          await AsyncStorage.setItem('paramAccount', JSON.stringify(params));

          if (onSetItem) {
            if (onBackup) {
              params.private = privateKey;
              params.type = 0;
              await onBackup(params);
            }
            await onSetItem(params);
          }
          await navigateBackup(params);
          this.setState({
            password: '',
            confirmPassword: '',
            isLoading: false,
          });
        }
      } catch (error) {
        this.setState({
          isLoading: false,
        });
      }
    }
  };

  handleRecover = async () => {
    const {onSetItem, onBackup} = this.props;
    const {privateKey, password} = this.state;
    const params = {};
    let account = {};
    try {
      account = await new wallet.Account(privateKey);
      await account.decrypt(password);
      if (account && account.privateKey) {
        const {privateKey, address} = account;
        params.address = address;
        params.privateKey = privateKey;
        await AsyncStorage.setItem('paramAccount', JSON.stringify(params));
        if (onSetItem) {
          if (onBackup) {
            params.private = privateKey;
            params.type = 1;
            await onBackup(params);
          }
          await onSetItem(params);
          await navigateToHome(params);
        }
      }
    } catch (e) {
      Alert.alert(
        `Hmm, that's not the right password and key. Please try again`,
      );
    }
  };

  renderCreate = () => {
    const {error, errorConfirm, password, confirmPassword} = this.state;

    return (
      <Block>
        <Block width={width * 0.8}>
          <Input
            password
            viewPass
            placeholder="Password"
            style={styles.inputs}
            iconContent={
              <Icon
                size={16}
                color="#ADB5BD"
                name="ios-unlock"
                style={styles.inputIcons}
              />
            }
            onChangeText={value => this.onChangePassword(value)}
            help={<Text style={styles.bottomHelp}>{error}</Text>}
            bottomHelp
            value={password}
          />
        </Block>
        <Block width={width * 0.8}>
          <Input
            password
            viewPass
            placeholder="Confirm password"
            style={styles.inputs}
            onChangeText={value => this.onChangeConfirmPassword(value)}
            help={<Text style={styles.bottomHelp}>{errorConfirm}</Text>}
            bottomHelp
            iconContent={
              <Icon
                size={16}
                color="#ADB5BD"
                name="ios-unlock"
                style={styles.inputIcons}
              />
            }
            value={confirmPassword}
          />
        </Block>
      </Block>
    );
  };

  renderRecover = () => {
    const {privateKey, password} = this.state;
    return (
      <Block flex={0.4} middle space="between">
        <Block width={width * 0.8}>
          <View style={styles.MainContainer}>
            <TextInput
              style={styles.TextInputStyleClass}
              underlineColorAndroid="transparent"
              placeholder={'Fill in the private key.'}
              placeholderTextColor={'#9E9E9E'}
              numberOfLines={4}
              multiline={true}
              onChangeText={text => this.onChangeText(text)}
              value={privateKey}
            />
          </View>
        </Block>
        <Block width={width * 0.8}>
          <View style={styles.MainContainer}>
            <Input
              password
              viewPass
              placeholder="Password"
              style={styles.inputs}
              iconContent={
                <Icon
                  size={16}
                  color="#ADB5BD"
                  name="ios-unlock"
                  style={styles.inputIcons}
                />
              }
              onChangeText={value => this.onChangePassword(value)}
              value={password}
            />
          </View>
        </Block>
      </Block>
    );
  };
  render() {
    const {
      error,
      errorConfirm,
      password,
      confirmPassword,
      isLoading,
    } = this.state;
    const {navigation} = this.props;
    const {id} = navigation.state.params;
    return (
      <DismissKeyboard>
        <Block flex middle>
          <ImageBackground
            source={Images.RegisterBackground}
            style={styles.imageBackgroundContainer}
            imageStyle={styles.imageBackground}>
            <Block flex middle>
              <Block style={styles.registerContainer}>
                <Block flex space="evenly">
                  <Block flex={0.3} middle style={styles.socialConnect}>
                    <Block flex={0.4} middle>
                      <Text
                        style={{
                          fontFamily: 'montserrat-regular',
                          textAlign: 'center',
                          fontWeight: 'bold',
                        }}
                        color="#333"
                        size={24}>
                        {id === 1 ? 'Create New Wallet' : 'Recover Wallet'}
                      </Text>
                    </Block>
                  </Block>

                  <Block flex={0.8} middle space="between">
                    <Block flex={0.8} space="between">
                      {id === 1 ? this.renderCreate() : this.renderRecover()}
                      {id === 1 ? (
                        <Block center>
                          <View>
                            <Button
                              color="primary"
                              round
                              disabled={
                                (error && errorConfirm) !== '' ||
                                password.length === 0 ||
                                confirmPassword.length === 0 ||
                                isLoading === true
                              }
                              loading={isLoading}
                              style={styles.createButton}
                              onPress={this.handleBackup}>
                              Continue
                            </Button>
                          </View>
                        </Block>
                      ) : (
                        <Block center>
                          <Button
                            color="primary"
                            round
                            style={styles.createButton}
                            onPress={this.handleRecover}>
                            <Text
                              style={{fontFamily: 'Montserrat-BoldItalic'}}
                              size={14}
                              color={nowTheme.COLORS.WHITE}>
                              Continue
                            </Text>
                          </Button>
                        </Block>
                      )}
                    </Block>
                  </Block>
                </Block>
              </Block>
            </Block>
          </ImageBackground>
        </Block>
      </DismissKeyboard>
    );
  }
}

const mapStateToProps = state => {
  console.log(TAG + ' mapStateToProps ' + JSON.stringify(state));
  return {
    dataItem: state,
  };
};

const mapDispatchToProps = dispatch => {
  console.log(TAG + ' mapDispatchToProps ');
  return {
    onSetItem: params => dispatch(itemAction.setItem(params)),
    onBackup: params => dispatch(itemAction.backUp(params)),
  };
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Account);
