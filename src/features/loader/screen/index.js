import React, {Component} from 'react';
import {ActivityIndicator} from 'react-native';

import SplashScreenNative from 'react-native-splash-screen';

const TAG = 'LoaderScreen';

export default class LoaderScreen extends Component {
  componentDidMount() {
    console.log(TAG + ' hide SplashScreen');
    SplashScreenNative.hide();
  }

  render() {
    return <ActivityIndicator style={{flex: 1}} />;
  }
}
