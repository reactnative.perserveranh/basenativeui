/*This is an Example of Searchable Dropdown*/
import React, {Component} from 'react';
//import react in our project
import {View, Text} from 'react-native';
//import basic react native components
import SearchableDropdown from 'react-native-searchable-dropdown';
//import SearchableDropdown component

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      serverData: [],
      //Data Source for the SearchableDropdown
    };
  }
  componentDidMount() {
    fetch(
      'https://aboutreact.000webhostapp.com/demo/webservice/demosearchables.php',
    )
      .then(response => response.json())
      .then(responseJson => {
        //Successful response from the API Call
        this.setState({
          serverData: [...this.state.serverData, ...responseJson.results],
          //adding the new data in Data Source of the SearchableDropdown
        });
      })
      .catch(error => {
        console.error(error);
      });
  }
  render() {
    return (
      <View style={{flex: 1, marginTop: 30}}>
        <Text style={{marginLeft: 10}}>News</Text>
      </View>
    );
  }
}
