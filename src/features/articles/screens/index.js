import React from 'react';
import {
  ScrollView,
  ActivityIndicator,
  View,
  TouchableOpacity,
  Dimensions,
  Alert,
} from 'react-native';
import {PricingCard} from 'react-native-elements';
//galio
import {Block, Text, theme} from 'galio-framework';
import SearchableDropdown from 'react-native-searchable-dropdown';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {Button, Select, Input, Header, Switch} from '../../../components';
import nowTheme from '../../../constants/Theme';
import styles from './style';

const {default: Neon, api, wallet} = require('@cityofzion/neon-js');
const width = Dimensions.get('screen').width;

const TAG = 'SENTOKEN';
const network = 'MainNet';

class Articles extends React.Component {
  state = {
    primaryFocus: false,
    primaryFocus2: false,
    isLoading: false,
    receivingAddress: '',
    amount: '',
    errorAddress: '',
    errorAmount: '',
    token: 'NEO',
    selectedItems: {},
    tokenAmount: 0,
  };
  showLoader = () => {};

  sendToken = async () => {
    const {
      receivingAddress,
      amount,
      selectedToken,
      errorAddress,
      errorAmount,
    } = this.state;
    const {privateKey} = this.props.dataItem;
    const sendingKey = privateKey;
    if (errorAddress === '' && errorAmount === '') {
      this.setState({isLoading: true});
      try {
        const intent = await api.makeIntent(
          {[`${selectedToken}`]: amount},
          receivingAddress,
        );
        const apiProvider = await new api.neoscan.instance(network);

        const account = await new wallet.Account(sendingKey);

        const config = {
          api: apiProvider, // The API Provider that we rely on for balance and rpc information
          account, // The sending Account
          intents: intent, // Our sending intents
        };

        Neon.sendAsset(config)
          .then(config => {
            console.log('\n\n--- Response ---');
            console.log(config.response);

            Alert.alert('Send amount success !');
            const total = selectedToken - amount;
            this.setState(previousState => ({
              ...previousState,
              isLoading: false,
              selectedToken: total,
            }));
          })
          .catch(error => {
            Alert.alert(
              `Hmm, that's not the right address and amount. Please try again`,
            );
            this.setState(previousState => ({
              ...previousState,
              isLoading: false,
            }));
          });
      } catch (error) {
        Alert.alert(
          `Hmm, that's not the right address and amount. Please try again`,
        );
        this.setState(previousState => ({
          ...previousState,
          isLoading: false,
        }));
      }
    }
  };

  handleChangeToken = value => {
    // console.log('1234', value);
  };

  handleSelectItem = item => {
    this.setState({
      selectedItems: item,
      token: item.name,
      tokenAmount: item.amount,
    });
  };

  onChangeAmount = value => {
    this.setState({
      amount: value,
    });
  };

  onChangeAddress = value => {
    this.setState({
      receivingAddress: value,
    });
  };

  render() {
    const {
      receivingAddress,
      amount,
      errorAddress,
      errorAmount,
      token,
      isLoading,
      tokenAmount,
    } = this.state;
    const items = [];
    const {dataTokens} = this.props;
    dataTokens.map((el, index) => {
      console.log('el', el);
      const item = {};
      item.id = index + 1;
      item.name = el.asset_symbol;
      item.amount = el.amount;
      item.asset = el.asset;
      items.push(item);
    });
    return (
      <Block flex center>
        <PricingCard
          containerStyle={{width: width / 1.25}}
          color="#4f9deb"
          title={'Token ' + `${token}`}
          price={`${tokenAmount}` + '$'}
          info={['User Wallet', 'Support Wallet', 'NEP 5 Token']}
          button=""
        />
        <SearchableDropdown
          onTextChange={value => this.handleChangeToken(value)}
          onItemSelect={this.handleSelectItem}
          containerStyle={{padding: 5, color: nowTheme.COLORS.WHITE}}
          textInputStyle={{
            width: width / 2,
            backgroundColor: nowTheme.COLORS.BACKGROUNDHEADER,
            paddingHorizontal: 18,
            paddingTop: 11,
            paddingBottom: 11,
            marginTop: 9,
            borderRadius: 4,
            shadowColor: 'rgba(0, 0, 0, 0.1)',
            shadowOffset: {width: 0, height: 2},
            shadowRadius: 4,
            shadowOpacity: 1,
            color: nowTheme.COLORS.WHITE,
            fontWeight: 'bold',
          }}
          itemStyle={{
            padding: 10,
            marginTop: 2,
            borderColor: '#bbb',
            borderWidth: 1,
            color: nowTheme.COLORS.WHITE,
          }}
          itemTextStyle={{
            color: nowTheme.COLORS.BACKGROUNDHEADER,
          }}
          itemsContainerStyle={{
            // maxHeight: '60%',
            color: nowTheme.COLORS.WHITE,
          }}
          items={items}
          // defaultIndex={1}
          textInputProps={{
            placeholder: 'Please select Token ...',
            underlineColorAndroid: 'transparent',
            style: {
              width: width * 0.8,
              padding: 12,
              borderWidth: 1,
              borderColor: '#ccc',
              borderRadius: 5,
              marginTop: 15,
              fontWeight: 'bold',
            },
            onTextChange: text => alert(text),
          }}
          underlineColorAndroid="transparent"
          resetValue={false}
          listProps={{
            nestedScrollEnabled: true,
          }}
        />
        <Block center style={{marginBottom: 10}}>
          <Input
            type="decimal-pad"
            primary={this.state.primaryFocus}
            right
            placeholder="Amount"
            onFocus={() => this.setState({primaryFocus: true})}
            onBlur={() => this.setState({primaryFocus: false})}
            iconContent={<Block />}
            shadowless
            onChangeText={value => this.onChangeAmount(value)}
            help={<Text style={styles.bottomHelp}>{errorAmount}</Text>}
            bottomHelp
            value={amount}
            style={{width: width * 0.8}}
          />
        </Block>
        <Block center style={{marginBottom: 10}}>
          <Input
            primary={this.state.primaryFocus2}
            right
            placeholder="Address"
            onFocus={() => this.setState({primaryFocus2: true})}
            onBlur={() => this.setState({primaryFocus2: false})}
            iconContent={<Block />}
            shadowless
            onChangeText={value => this.onChangeAddress(value)}
            help={<Text style={styles.bottomHelp}>{errorAddress}</Text>}
            bottomHelp
            value={receivingAddress}
            style={{width: width * 0.8}}
          />
        </Block>
        <Block center>
          <Button
            color="info"
            disabled={
              (errorAddress && errorAmount) !== '' ||
              receivingAddress.length === 0 ||
              amount.length === 0
            }
            size="large"
            color="info"
            loading={isLoading}
            onPress={this.sendToken}
            style={styles.button}>
            SEND
          </Button>
        </Block>
      </Block>
    );
  }
}

const mapStateToProps = state => {
  console.log(TAG + ' mapStateToProps ' + JSON.stringify(state));
  return {
    dataTokens: state.default.dataTokens,
    dataItem: state.default.dataItem.param,
  };
};

const mapDispatchToProps = dispatch => {
  console.log(TAG + ' mapDispatchToProps ');
  return {};
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Articles);
