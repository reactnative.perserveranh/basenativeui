import React from 'react';
import {Dimensions, ScrollView, Image, ImageBackground} from 'react-native';
import {Block, Text, theme, Button as GaButton} from 'galio-framework';

import {Button} from '../../../components';
import {Images, nowTheme} from '../../../constants';
import {HeaderHeight} from '../../../constants/utils';

const {width, height} = Dimensions.get('screen');

import styles from './style';

const Profile = () => {
  return (
    <Block
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
      }}>
      <Block flex={0.6}>
        <ImageBackground
          source={Images.ProfileBackground}
          style={styles.profileContainer}
          imageStyle={styles.profileBackground}>
          <Block flex style={styles.profileCard}>
            <Block
              style={{
                position: 'absolute',
                width: width,
                zIndex: 5,
                paddingHorizontal: 20,
              }}>
              <Block middle style={{top: height * 0.15}}>
                <Image source={Images.ProfilePicture} style={styles.avatar} />
              </Block>
              <Block style={{top: height * 0.2}}>
                <Block middle>
                  <Text
                    style={{
                      fontFamily: 'Montserrat-BoldItalic',
                      marginBottom: theme.SIZES.BASE / 2,
                      fontWeight: '900',
                      fontSize: 26,
                    }}
                    color="#ffffff">
                    Ryan Scheinder
                  </Text>

                  <Text
                    size={16}
                    color="white"
                    style={{
                      marginTop: 5,
                      fontFamily: 'Montserrat-BoldItalic',
                      lineHeight: 20,
                      fontWeight: 'bold',
                      fontSize: 18,
                      opacity: 0.8,
                    }}>
                    Photographer
                  </Text>
                </Block>
                <Block style={styles.info}>
                  <Block row space="around">
                    <Block middle>
                      <Text
                        size={18}
                        color="white"
                        style={{
                          marginBottom: 4,
                          fontFamily: 'Montserrat-BoldItalic',
                        }}>
                        2K
                      </Text>
                      <Text
                        style={{fontFamily: 'montserrat-regular'}}
                        size={14}
                        color="white">
                        Friends
                      </Text>
                    </Block>

                    <Block middle>
                      <Text
                        color="white"
                        size={18}
                        style={{
                          marginBottom: 4,
                          fontFamily: 'Montserrat-BoldItalic',
                        }}>
                        26
                      </Text>
                      <Text
                        style={{fontFamily: 'montserrat-regular'}}
                        size={14}
                        color="white">
                        Comments
                      </Text>
                    </Block>

                    <Block middle>
                      <Text
                        color="white"
                        size={18}
                        style={{
                          marginBottom: 4,
                          fontFamily: 'Montserrat-BoldItalic',
                        }}>
                        48
                      </Text>
                      <Text
                        style={{fontFamily: 'montserrat-regular'}}
                        size={14}
                        color="white">
                        Bookmarks
                      </Text>
                    </Block>
                  </Block>
                </Block>
              </Block>
            </Block>

            <Block
              middle
              row
              style={{
                position: 'absolute',
                width: width,
                top: height * 0.6 - 22,
                zIndex: 99,
              }}>
              <Button
                style={{
                  width: 114,
                  height: 44,
                  marginHorizontal: 5,
                  elevation: 0,
                }}
                textStyle={{fontSize: 16}}
                round>
                Follow
              </Button>
              <GaButton
                round
                onlyIcon
                shadowless
                icon="twitter"
                iconFamily="Font-Awesome"
                iconColor={nowTheme.COLORS.WHITE}
                iconSize={nowTheme.SIZES.BASE * 1.375}
                color={'#888888'}
                style={[styles.social, styles.shadow]}
              />
              <GaButton
                round
                onlyIcon
                shadowless
                icon="pinterest"
                iconFamily="Font-Awesome"
                iconColor={nowTheme.COLORS.WHITE}
                iconSize={nowTheme.SIZES.BASE * 1.375}
                color={'#888888'}
                style={[styles.social, styles.shadow]}
              />
            </Block>
          </Block>
        </ImageBackground>
      </Block>
      <Block />
      <Block flex={0.4} style={{padding: theme.SIZES.BASE, marginTop: 90}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Block flex style={{marginTop: 20}}>
            <Block middle>
              <Text
                style={{
                  color: '#2c2c2c',
                  fontWeight: 'bold',
                  fontSize: 19,
                  fontFamily: 'Montserrat-BoldItalic',
                  marginTop: 15,
                  marginBottom: 30,
                  zIndex: 2,
                }}>
                About me
              </Text>
              <Text
                size={16}
                muted
                style={{
                  textAlign: 'center',
                  fontFamily: 'montserrat-regular',
                  zIndex: 2,
                  lineHeight: 25,
                  color: '#9A9A9A',
                  paddingHorizontal: 15,
                }}>
                An artist of considerable range, named Ryan — the name has taken
                by Melbourne has raised, Brooklyn-based Nick Murphy — writes,
                performs and records all of his own music.
              </Text>
            </Block>
            <Block
              row
              style={{paddingVertical: 14, paddingHorizontal: 15}}
              space="between">
              <Text bold size={16} color="#2c2c2c" style={{marginTop: 3}}>
                Album
              </Text>
              <Button
                small
                color="transparent"
                textStyle={{color: nowTheme.COLORS.PRIMARY, fontSize: 14}}>
                View all
              </Button>
            </Block>

            <Block
              style={{paddingBottom: -HeaderHeight * 2, paddingHorizontal: 15}}>
              <Block row space="between" style={{flexWrap: 'wrap'}}>
                {Images.Viewed.map((img, imgIndex) => (
                  <Image
                    source={img}
                    key={`viewed-${img}`}
                    resizeMode="cover"
                    style={styles.thumb}
                  />
                ))}
              </Block>
            </Block>
          </Block>
        </ScrollView>
      </Block>
    </Block>
  );
};

export default Profile;
