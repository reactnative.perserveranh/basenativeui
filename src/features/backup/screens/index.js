import React from 'react';
import {
  ImageBackground,
  Dimensions,
  TouchableWithoutFeedback,
  Keyboard,
  View,
  TextInput,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {Block, Text, Card} from 'galio-framework';

import {Button, Input} from '../../../components';
import {Images, nowTheme} from '../../../constants';
import getIconType from '../../../helper/getIconType';
import {navigateToHome} from '../../../navigation/NavigationHelpers';
const Icon = getIconType('Ionicon');
const {width} = Dimensions.get('screen');
import styles from './style';

const TAG = 'BACKUP';

const DismissKeyboard = ({children}) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class Backup extends React.Component {
  handleWallet = () => {
    navigateToHome({id: 1});
  };

  render() {
    const {navigation, dataItem} = this.props;
    const {key} = navigation.state.params;
    const value = key || dataItem.param.key;
    return (
      <DismissKeyboard>
        <Block flex middle>
          <ImageBackground
            source={Images.RegisterBackground}
            style={styles.imageBackgroundContainer}
            imageStyle={styles.imageBackground}>
            <Block flex middle>
              <Block style={styles.registerContainer}>
                <Block flex space="evenly">
                  <Block flex={0.7} middle style={styles.socialConnect}>
                    <Block flex={0.3} middle>
                      <Text
                        style={{
                          textAlign: 'center',
                          fontWeight: 'bold',
                        }}
                        color="#333"
                        size={24}>
                        Backup Key
                      </Text>
                      <Block
                        style={styles.wrapperText}
                        center
                        width={width * 0.8}
                        card
                        middle>
                        <Text bold muted italic size={14}>
                          Please store and remember your "keystore", "password"
                          and "key" to access your wallet.
                        </Text>
                      </Block>
                    </Block>
                  </Block>

                  <Block flex={1} middle space="between">
                    <Block center flex={0.7}>
                      <Block flex space="between">
                        <Block>
                          <Block width={width * 0.8}>
                            <View style={styles.MainContainer}>
                              <TextInput
                                style={styles.TextInputStyleClass}
                                underlineColorAndroid="transparent"
                                placeholderTextColor={'#9E9E9E'}
                                numberOfLines={4}
                                multiline={true}
                                editable={false}
                                value={value}
                              />
                            </View>
                          </Block>
                        </Block>
                        <Block center>
                          <Button
                            color="primary"
                            round
                            style={styles.createButton}
                            onPress={this.handleWallet}>
                            <Text
                              style={{fontFamily: 'Montserrat-BoldItalic'}}
                              size={14}
                              color={nowTheme.COLORS.WHITE}>
                              Continue
                            </Text>
                          </Button>
                        </Block>
                      </Block>
                    </Block>
                  </Block>
                </Block>
              </Block>
            </Block>
          </ImageBackground>
        </Block>
      </DismissKeyboard>
    );
  }
}

const mapStateToProps = state => {
  console.log(TAG + ' mapStateToProps ' + JSON.stringify(state));
  return {
    dataItem: state,
  };
};

const mapDispatchToProps = dispatch => {
  console.log(TAG + ' mapDispatchToProps ');
  return {
    onSetItem: params => dispatch(itemAction.setItem(params)),
  };
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(withConnect)(Backup);
