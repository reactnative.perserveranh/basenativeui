import * as types from '../constants';

const stateSeeds = {
  dataItem: {},
  showLoading: false,
  showSidebar: false,
  statusBackup: {},
  dataTokens: [],
};

const setItemReduce = (state = stateSeeds, action) => {
  switch (action.type) {
    case types.SHOW_LOADING: {
      return {
        ...state,
        showLoading: true,
      };
    }
    case types.HIDE_LOADING: {
      return {
        ...state,
        showLoading: false,
      };
    }

    case types.SET_ITEM: {
      return {
        ...state,
      };
    }

    case types.SET_ITEM_SUCCESS: {
      return {
        ...state,
        dataItem: {...action.payload},
      };
    }

    case types.SET_TOKEN: {
      return {
        ...state,
      };
    }

    case types.SET_TOKEN_SUCCESS: {
      const {dataTokens} = action.payload.param;
      return {
        ...state,
        dataTokens: [...dataTokens],
      };
    }

    case types.BACKUP: {
      return {
        ...state,
      };
    }
    case types.BACKUP_SUCCESS: {
      return {
        ...state,
        statusBackup: {...action.payload.data},
      };
    }
    case types.BACKUP_FAILED: {
      return {
        ...state,
        statusBackup: {...action.payload.error},
      };
    }

    case types.RESET_STORE: {
      return {
        ...state,
        dataItem: {},
      };
    }

    default:
      return state;
  }
};

export default setItemReduce;
