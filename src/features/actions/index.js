import * as types from '../constants';

export const showLoading = () => ({
  type: uiTypes.SHOW_LOADING,
});

export const hideLoading = () => ({
  type: uiTypes.HIDE_LOADING,
});

export const getData = param => ({
  type: types.GETDATA,
  payload: {
    param,
  },
});

export const getdataSuccess = data => ({
  type: types.GETDATA_SUCCESS,
  payload: {
    data,
  },
});

export const getdataFailed = error => ({
  type: types.GETDATA_FAILED,
  payload: {
    error,
  },
});

export const backUp = param => ({
  type: types.BACKUP,
  payload: {
    param,
  },
});

export const backUpSuccess = data => ({
  type: types.BACKUP_SUCCESS,
  payload: {
    data,
  },
});

export const backUpFailed = error => ({
  type: types.BACKUP_FAILED,
  payload: {
    error,
  },
});

export const setItem = param => ({
  type: types.SET_ITEM,
  payload: {
    param,
  },
});

export const setItemSuccess = param => ({
  type: types.SET_ITEM_SUCCESS,
  payload: {
    param,
  },
});

export const setToken = param => ({
  type: types.SET_TOKEN,
  payload: {
    param,
  },
});

export const setTokenSuccess = param => ({
  type: types.SET_TOKEN_SUCCESS,
  payload: {
    param,
  },
});

export const resetStore = param => ({
  type: types.RESET_STORE,
  payload: {
    param,
  },
});
