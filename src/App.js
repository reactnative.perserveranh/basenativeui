import React from 'react';
import {Block, GalioProvider} from 'galio-framework';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/es/integration/react';
import Screens from './navigation';
import {Images, articles, nowTheme} from './constants';
import configureStore from './stores/configureStore';
import LoaderScreen from './features/loader/screen';

const {persistor, store} = configureStore();
export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
    fontLoaded: false,
  };

  render() {
    return (
      // <SafeAreaProvider>
      // <GalioProvider theme={nowTheme}>
      <Provider store={store}>
        <PersistGate loading={<LoaderScreen />} persistor={persistor}>
          <Block flex>
            <Screens />
          </Block>
        </PersistGate>
      </Provider>
      // </GalioProvider>
      // </SafeAreaProvider>
    );
  }
}
