import React from 'react';
import {withNavigation} from 'react-navigation';
import {TouchableOpacity, StyleSheet, Platform, Dimensions} from 'react-native';
import {Header as Head, Icon} from 'react-native-elements';
import SafeAreaView from 'react-native-safe-area-view';
import nowTheme from '../constants/Theme';
import {navigateGoback} from '../navigation/NavigationHelpers';
// import getIconType from '../helper/getIconType';

// const Icon = getIconType('Ionicon');

const {height, width} = Dimensions.get('window');

class Header extends React.Component {
  handleLeftPress = () => {
    const {back, navigation, goBack} = this.props;
    const {navigate} = navigation;
    if (goBack) {
      navigateGoback();
    } else if (back) {
      navigate('Login');
    } else {
      navigation.openDrawer();
    }
  };

  centerComponent = () => {
    const {white, title, navigation, back} = this.props;
    const {routeName} = navigation.state;
    switch (routeName) {
      case 'Home':
        return {
          text: 'HOME',
          style: {color: nowTheme.COLORS.WHITE},
        };

      case 'Account':
        return {
          text: 'Create New Wallet',
          style: {
            color: nowTheme.COLORS.WHITE,
            fontSize: 18,
            fontWeight: '700',
          },
        };

      case 'Key':
        return {
          text: 'Unlock With Key',
          style: {
            color: nowTheme.COLORS.WHITE,
            fontSize: 18,
            fontWeight: '700',
          },
        };

      case 'CreatePassword':
        return {
          text: 'Create Password',
          style: {
            color: nowTheme.COLORS.WHITE,
            fontSize: 18,
            fontWeight: '700',
          },
        };

      case 'Articles':
        return {
          text: 'SEND TOKEN',
          style: {color: nowTheme.COLORS.WHITE},
        };
      case 'Settings':
        return {
          text: 'RECEIVE QRCode',
          style: {color: nowTheme.COLORS.WHITE},
        };

      case 'Components':
        return {
          text: 'NEWS',
          style: {color: nowTheme.COLORS.WHITE},
        };
      default:
        break;
    }
  };

  leftComponent = () => {
    const {white, title, navigation, back} = this.props;
    const {routeName} = navigation.state;
    switch (routeName) {
      case 'Home':
        return {
          text: 'HOME',
          style: {color: '#fff'},
        };

      case 'Articles':
        return {
          text: 'SEND TOKEN',
          style: {color: '#fff'},
        };
      case 'Settings':
        return {
          text: 'RECEIVE QRCode',
          style: {color: '#fff'},
        };
      default:
        break;
    }
  };

  rightComponent = () => {
    const {white, title, navigation, back} = this.props;
    const {routeName} = navigation.state;
    switch (routeName) {
      case 'Home':
        return <Icon name="home" color={nowTheme.COLORS.WHITE} />;
      case 'Articles':
        return <Icon name="trending-up" color={nowTheme.COLORS.WHITE} />;
      case 'Settings':
        return (
          <Icon
            name="ios-barcode"
            type="ionicon"
            color={nowTheme.COLORS.WHITE}
          />
        );
      case 'Components':
        return (
          <Icon
            name="ios-bookmarks"
            type="ionicon"
            color={nowTheme.COLORS.WHITE}
          />
        );
      default:
        break;
    }
  };

  render() {
    const {
      back,
      title,
      white,
      transparent,
      bgColor,
      iconColor,
      titleColor,
      navigation,
      ...props
    } = this.props;

    return (
      <Head
        barStyle="light-content"
        leftComponent={
          <Icon
            name={back ? 'back' : 'menuunfold'}
            color={nowTheme.COLORS.WHITE}
            type="antdesign"
            onPress={this.handleLeftPress}
          />
        }
        centerComponent={this.centerComponent()}
        rightComponent={this.rightComponent()}
        containerStyle={{
          backgroundColor: nowTheme.COLORS.BACKGROUNDHEADER,
          justifyContent: 'space-around',
          height: Platform.OS === 'android' ? 50 : 100,
          paddingTop: Platform.OS === 'android' ? -10 : 50,
          // paddingBottom: Platform.OS === 'android' ? 20 : 0,
        }}
      />
    );
  }
}

export default withNavigation(Header);
