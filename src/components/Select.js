import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import PropTypes from 'prop-types';
import ModalDropdown from 'react-native-modal-dropdown';
import {Block, Text} from 'galio-framework';

import getIconType from '../helper/getIconType';
import {nowTheme} from '../constants';

const Icon = getIconType('Ionicon');

const width = Dimensions.get('screen').width;

class DropDown extends React.Component {
  state = {
    value: 1,
  };

  handleOnSelect = (index, value) => {
    const {onSelect} = this.props;

    this.setState({value: value});
    onSelect && onSelect(index, value);
  };

  render() {
    const {
      onSelect,
      iconName,
      iconFamily,
      iconSize,
      iconColor,
      color,
      textStyle,
      style,
      ...props
    } = this.props;

    const modalStyles = [styles.qty, color && {backgroundColor: color}, style];

    const textStyles = [styles.text, textStyle];

    return (
      <ModalDropdown
        style={modalStyles}
        onSelect={this.handleOnSelect}
        dropdownStyle={styles.dropdown}
        dropdownTextStyle={{paddingLeft: 16, fontSize: 16}}
        {...props}>
        <Block flex row middle space="between">
          <Text size={18} style={textStyles}>
            {this.state.value}
          </Text>
          <Icon
            name={iconName || 'ios-arrow-down'}
            size={iconSize || 10}
            color={iconColor || nowTheme.COLORS.WHITE}
          />
        </Block>
      </ModalDropdown>
    );
  }
}

DropDown.propTypes = {
  onSelect: PropTypes.func,
  iconName: PropTypes.string,
  iconFamily: PropTypes.string,
  iconSize: PropTypes.number,
  color: PropTypes.string,
  textStyle: PropTypes.any,
};

const styles = StyleSheet.create({
  qty: {
    width: width / 3,
    backgroundColor: nowTheme.COLORS.PRIMARY,
    paddingHorizontal: 18,
    paddingTop: 11,
    paddingBottom: 11,
    marginTop: 9,
    borderRadius: 4,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 4,
    shadowOpacity: 1,
  },
  text: {
    color: nowTheme.COLORS.WHITE,
    fontWeight: '600',
  },
  dropdown: {
    marginTop: 25,
    marginLeft: 0,
    width: 100,
  },
});

export default DropDown;
