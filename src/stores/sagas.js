import {all, fork} from 'redux-saga/effects';
import {loginSagas} from '../features/login/sagas';
import itemSaga from '../features/sagas';
// import {homeSagas} from '../features/home/sagas';

export default function* rootSaga() {
  yield all([...loginSagas]); // all([...otherSaga])
  // yield all([...homeSagas]);
  yield all([yield fork(itemSaga)]);
}
