import {combineReducers} from 'redux';
import * as loginReducer from '../features/login/reducers';
import * as itemReducer from '../features/reducers';
// import * as homeReducer from '../features/home/reducers';

export default Object.assign({}, loginReducer, itemReducer);

// const rootReducer = combineReducers({
//   loginReducer, //  Please add your reducer here
// });

// export default rootReducer;
