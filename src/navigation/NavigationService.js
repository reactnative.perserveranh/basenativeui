import {NavigationActions, StackActions} from 'react-navigation';
import {DrawerActions} from 'react-navigation-drawer';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params = {}) {
  _navigator.dispatch(NavigationActions.navigate({routeName, params}));
}

function goBack(key) {
  _navigator.dispatch(NavigationActions.back({key: key}));
}

function replaceScreen(routeName, params) {
  _navigator.dispatch(StackActions.replace({routeName, params}));
}

function toggleDrawer() {
  _navigator.dispatch(DrawerActions.toggleDrawer());
}

function openDrawer() {
  _navigator.dispatch(DrawerActions.openDrawer());
}

function closeDrawer() {
  _navigator.dispatch(DrawerActions.closeDrawer());
}
export default {
  navigate,
  goBack,
  replaceScreen,
  setTopLevelNavigator,
  toggleDrawer,
  openDrawer,
  closeDrawer,
};
