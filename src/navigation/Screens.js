import React from 'react';
import {Block} from 'galio-framework';
import {Easing, Animated, Platform} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createBottomTabNavigator} from 'react-navigation-tabs';
// import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import getIconType from '../helper/getIconType';
// screens
import Home from '../features/home/screens';
import Profile from '../features/profile/screens';
import Account from '../features/account/screens';
import Components from '../features/components/screens';
import Articles from '../features/articles/screens';
import Login from '../features/login/screens';
import Key from '../features/key/screens';
import Backup from '../features/backup/screens';
import CreatePassword from '../features/createPassword/screens';

// settings
import SettingsScreen from '../features/setting/screens';

// drawer
import Menu from './Menu';
import DrawerItem from '../components/DrawerItem';

// header for screens
import Header from '../components/Header';

const Icon = getIconType('Ionicon');

const transitionConfig = (transitionProps, prevTransitionProps) => ({
  transitionSpec: {
    duration: 400,
    easing: Easing.out(Easing.poly(4)),
    timing: Animated.timing,
  },
  screenInterpolator: sceneProps => {
    const {layout, position, scene} = sceneProps;
    const thisSceneIndex = scene.index;
    const width = layout.initWidth;

    const scale = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
      outputRange: [4, 1, 1],
    });
    const opacity = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
      outputRange: [0, 1, 1],
    });
    const translateX = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex],
      outputRange: [width, 0],
    });

    const scaleWithOpacity = {scale};
    const screenName = 'Search';

    if (
      screenName === transitionProps.scene.route.routeName ||
      (prevTransitionProps &&
        screenName === prevTransitionProps.scene.route.routeName)
    ) {
      return scaleWithOpacity;
    }
    return {transform: [{translateX}]};
  },
});

const ComponentsStack = createStackNavigator(
  {
    Components: {
      screen: Components,
      navigationOptions: ({navigation}) => ({
        header: <Header title="News" navigation={navigation} />,
      }),
    },
  },
  {
    cardStyle: {
      backgroundColor: '#FFFFFF',
    },
    transitionConfig,
  },
);

const SettingsStack = createStackNavigator(
  {
    Settings: {
      screen: SettingsScreen,
      navigationOptions: ({navigation}) => ({
        header: <Header title="Settings" navigation={navigation} />,
      }),
    },
  },
  {
    cardStyle: {backgroundColor: '#FFFFFF'},
    transitionConfig,
  },
);

const ArticlesStack = createStackNavigator(
  {
    Articles: {
      screen: Articles,
      navigationOptions: ({navigation}) => ({
        header: <Header title="Articles" navigation={navigation} />,
      }),
    },
  },
  {
    cardStyle: {
      backgroundColor: '#FFFFFF',
    },
    transitionConfig,
  },
);

const ProfileStack = createStackNavigator(
  {
    Profile: {
      screen: Profile,
      navigationOptions: ({navigation}) => ({
        header: null,
        headerTransparent: true,
      }),
    },
  },
  {
    cardStyle: {backgroundColor: '#FFFFFF'},
    transitionConfig,
  },
);

const AccountStack = createStackNavigator(
  {
    Account: {
      screen: Account,
      navigationOptions: ({navigation}) => ({
        header: null,
        headerTransparent: true,
      }),
    },
  },
  {
    cardStyle: {backgroundColor: '#FFFFFF'},
    transitionConfig,
  },
);

const CreatePasswordStack = createStackNavigator(
  {
    CreatePassword: {
      screen: CreatePassword,
      navigationOptions: ({navigation}) => ({
        header: null,
        headerTransparent: true,
      }),
    },
  },
  {
    cardStyle: {backgroundColor: '#FFFFFF'},
    transitionConfig,
  },
);

const KeyStack = createStackNavigator(
  {
    Key: {
      screen: Key,
      navigationOptions: ({navigation}) => ({
        header: null,
        headerTransparent: true,
      }),
    },
  },
  {
    cardStyle: {backgroundColor: '#FFFFFF'},
    transitionConfig,
  },
);

const BackupStack = createStackNavigator(
  {
    Backup: {
      screen: Backup,
      navigationOptions: ({navigation}) => ({
        header: null,
        headerTransparent: true,
      }),
    },
  },
  {
    cardStyle: {backgroundColor: '#FFFFFF'},
    transitionConfig,
  },
);

const LoginStack = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: ({navigation}) => ({
        header: null,
        headerTransparent: true,
      }),
    },
  },
  {
    cardStyle: {backgroundColor: '#FFFFFF'},
    transitionConfig,
  },
);

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: ({navigation}) => ({
        header: <Header title="Home" navigation={navigation} />,
      }),
    },
  },
  {
    cardStyle: {
      backgroundColor: '#FFFFFF',
    },
    transitionConfig,
  },
);

const MainTabs = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: 'Home',
        // activeTintColor: 'yellow',
      },
    },

    Atticle: {
      screen: ArticlesStack,
      navigationOptions: {
        tabBarLabel: 'Send Token',
      },
    },
    Setting: {
      screen: SettingsStack,
      navigationOptions: {
        tabBarLabel: 'Receive QRCode',
      },
    },
    Component: {
      screen: ComponentsStack,
      navigationOptions: {
        tabBarLabel: 'News',
      },
    },
  },
  {
    tabBarOptions: {
      activeTintColor: '#561f55',
      inactiveTintColor: '#2e279d',
      // activeBackgroundColor: 'blue',
      style: {
        // backgroundColor: 'transparent',
        fontSize: 18,
      },
    },
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        if (routeName === 'Home') {
          return <Icon name="ios-home" size={16} color="#46b3e6" />;
        } else if (routeName === 'Component') {
          return <Icon name="ios-bookmarks" size={16} color="#46b3e6" />;
        } else if (routeName === 'Atticle') {
          return <Icon name="ios-trending-up" size={16} color="#46b3e6" />;
        } else {
          return <Icon name="ios-barcode" size={16} color="#46b3e6" />;
        }
      },
    }),
  },
  {
    initialRouteName: 'Home',
  },
);

const AppStack = createDrawerNavigator(
  {
    Home: {
      screen: MainTabs,
      navigationOptions: navOpt => ({
        drawerLabel: ({focused}) => (
          <DrawerItem focused={focused} title="Home" screen="Home" />
        ),
      }),
    },
    Profile: {
      screen: ProfileStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({focused}) => (
          <DrawerItem focused={focused} screen="Profile" title="Profile" />
        ),
      }),
    },
  },
  {
    ...Menu,
  },
);

const App = createStackNavigator(
  {
    Drawer: {
      screen: AppStack,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
    // MainStack: {
    //   screen: MainTabs,
    // },
    Login: {
      screen: LoginStack,
      navigationOptions: ({navigation}) => ({
        header: null,
        drawerLockMode: 'locked-closed',
      }),
    },
    Account: {
      screen: AccountStack,
      navigationOptions: ({navigation}) => ({
        headerBackTitle: null,
        header: <Header back title="Create Wallet" navigation={navigation} />,
      }),
    },
    Key: {
      screen: KeyStack,
      navigationOptions: ({navigation}) => ({
        headerBackTitle: null,
        header: <Header back title="Unlock With Key" navigation={navigation} />,
      }),
    },
    Backup: {
      screen: BackupStack,
      navigationOptions: ({navigation}) => ({
        headerBackTitle: null,
        header: <Header back goBack title="Back Up" navigation={navigation} />,
      }),
    },
    CreatePassword: {
      screen: CreatePasswordStack,
      navigationOptions: ({navigation}) => ({
        headerBackTitle: null,
        header: (
          <Header back goBack title="Create Password" navigation={navigation} />
        ),
      }),
    },
  },
  {
    initialRouteName: 'Login',
  },
);

const AppContainer = createAppContainer(App);
export default AppContainer;
