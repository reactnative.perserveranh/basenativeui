import NavigationService from './NavigationService';

export function navigateToHome(params) {
  NavigationService.navigate('Home', params);
}

export function navigateToLogin(params) {
  NavigationService.navigate('Login', params);
}

export function navigateAccount(params) {
  NavigationService.navigate('Account', params);
}

export function navigateKey(params) {
  NavigationService.navigate('Key', params);
}

export function navigateBackup(params) {
  NavigationService.navigate('Backup', params);
}

export function navigateCreatePassword(params) {
  NavigationService.navigate('CreatePassword', params);
}

// export function navigateToSplash(params) {
//   NavigationService.navigate(ScreenTypes.Splash, params);
// }

export function navigateToSetting(params) {
  NavigationService.navigate('Setting', params);
}

export function navigateDrawer() {
  NavigationService.openDrawer();
}

export function navigateGoback() {
  NavigationService.goBack();
}
export function navigateToRecover(params) {
  NavigationService.navigate('Recover', params);
}

// export function navigateToDetailHeadlines(params) {
//   NavigationService.navigate(ScreenTypes.DetailTopHeadlines, params);
// }
